# ÖPNV Connector



## Beschreibung
Der ÖPNV Connector nutzt die Opendataschnittstelle für die in NRW verfügbaren Dienste der [Open-Service Schnittstelle der Fahrplanauskunft](https://www.opendata-oepnv.de/ht/de/api). Einzelne Haltestellen werden hier angefragt und alle aktuellen Verspätungen der dort verkehrenden Linien werden ausgelesen. Exemplarisch werden diese in einer MSSQL Tabelle gespeichert. 

## Installation
Dieser Flow basiert auf Node Red. In bestehende Node Red Instanzen kann die [VRRFlow.json] Datein über die Importfunktion aufgenommen werden. Anzupassen sind dann die Parameter des Initialisierungsknotens, um die entsprechenden Haltestellen abzufragen und den Rhytmus der Abfrage festzulegen.
Im Rahmen der Nutzung der Schnittstelle bitte die entsprechenden [Nutzungsbedingungen](https://www.opendata-oepnv.de/ht/de/standards/nutzungsbedingungen) und die [Nutzungsvereinbarung der API](https://www.opendata-oepnv.de/ht/de/standards/nutzungsvereinbarung-api) beachten.

## Support
Sollten bei der Wiederverwendung Probleme auftreten, öffnen Sie bitte entsprechende Issues in diesem Repository.

## Roadmap
- Verbesserung der Konfigurierbarkeit

## Contributing
Eine urbane Datenplattform erfordert die Zusammenarbeit zwischen Kommunen, um Entwicklungskapazitäten effizient einzusetzen und von den Erfolgen gemeinsam zu partizipieren. Daher sind alle Kommunen dazu eingeladen an den Flows mitzuentwickeln und eigene Flows für weitere Importe oder Dashboards einzubringen. Nur gemeinsam werden wir das Projekt skalieren können.

## Authors and acknowledgment
Stadt Oberhausen

## Project status
Active
